public class Main {
    public static void main(String[] args) {
        Agenda minhaAgenda = new Agenda();
        System.out.println("Minha agenda tem \n"+minhaAgenda.temQuantos());
        minhaAgenda.armazenarPessoa("Maria", 52, 1.65f );
        minhaAgenda.armazenarPessoa("Laura", 49, 1.68f );
        minhaAgenda.armazenarPessoa("Gustavo", 27, 1.76f );
        minhaAgenda.armazenarPessoa("Emerson", 26, 1.85f );
        minhaAgenda.armazenarPessoa("Taina", 26, 1.72f );
        minhaAgenda.armazenarPessoa("Manuela", 24, 1.70f );
        minhaAgenda.armazenarPessoa("Gabriela", 23, 1.66f );
        minhaAgenda.armazenarPessoa("Camila", 25, 1.72f );
        minhaAgenda.armazenarPessoa("Jadson", 35, 1.89f );
        minhaAgenda.armazenarPessoa("Marcelo", 49, 1.81f );

        System.out.println("Minha agenda tem\n"+minhaAgenda.temQuantos());
        minhaAgenda.imprimirAgenda();

        System.out.println("Achou Taina? "+minhaAgenda.buscarPessoa("Taina"));
        System.out.println("Achou a Pessoa? "+minhaAgenda.buscarPessoa("Pessoa"));
        if (minhaAgenda.removePessoa("")) {
            System.out.println("Removeu Taina\n");
            System.out.println("Minha agenda tem "+minhaAgenda.temQuantos());

        };
        System.out.println("*");
        minhaAgenda.imprimirAgenda();
        if (minhaAgenda.removePessoa("Marcelo")) {
            System.out.println("Removeu Marcelo");
        }else{
            System.out.println("Nao Conseguiu remover Marcelo");
        }
        System.out.println("*");
        minhaAgenda.imprimirAgenda();
        if (minhaAgenda.removePessoa("Taina")) {
            System.out.println("Removeu Taina");
        }else{
            System.out.println("Nao Conseguiu remover Taina");
        }
        System.out.println("*");
        minhaAgenda.imprimirAgenda();


    }
}

